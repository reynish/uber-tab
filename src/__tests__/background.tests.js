const bg = require("../background")

describe("ignoredUrls", () => {
    test("it should ignore urls", () => {
        const urls = [
            "chrome://extensions",
            "https://extensions",
        ]
        expect(bg.ignoredUrls(urls[0])).toBeFalsy()
    })
})