const syncGet = (keys) =>
    new Promise((resolve, reject) => {
        chrome.storage.sync.get(keys, ({ uberTabFolderId }) =>
            resolve(uberTabFolderId)
        );
    });

const getUberTabFolderId = () =>
    new Promise((resolve, reject) => {
        chrome.storage.sync.get(["uberTabFolderId"], ({ uberTabFolderId }) =>
            resolve(uberTabFolderId)
        );
    });

const setUberTabFolderId = (id) =>
    new Promise((resolve, reject) => {
        chrome.storage.sync.set({ uberTabFolderId: id }, () => {
            console.log(`uberTabFolderId = ${uberTabFolderId}`);
            resolve();
        });
    });

const createFolder = ({ title = "Untitled", parentId }) =>
    new Promise((resolve, reject) => {
        chrome.bookmarks.create({ title, parentId }, (node, result) =>
            resolve(node)
        );
    });

const createBookmark = (bm) =>
    new Promise((resolve, reject) => {
        chrome.bookmarks.create(bm, () => {
            resolve();
        });
    });

const queryTabs = () =>
    new Promise((resolve, reject) => {
        chrome.tabs.query({}, (tabs) => {
            resolve(tabs.filter((t) => ignoredUrls(t.url)));
        });
    });

export const ignoredUrls = (url) => {
    const urls = ["chrome://","brave://"];
    for (let i = 0; i < urls.length; i++) {
        if (url.startsWith(urls[i])) {
            return false;
        }
    };
    return true;
};

const createTab = (url) =>
    new Promise((resolve, reject) => {
        chrome.tabs.create({ index: 0, url }, (tab) => {
            resolve(tab);
        });
    });

chrome.runtime.onInstalled.addListener(async () => {
    const uberTabFolderId = await getUberTabFolderId();
    console.log(`uberTabFolderId = ${uberTabFolderId}`);
    if (!uberTabFolderId) {
        const newFolder = await createFolder({ title: "uberTab" });
        await setUberTabFolderId(newFolder.id);
    }
});

chrome.browserAction.onClicked.addListener(async (tab) => {

    const tabs = await queryTabs();

    if (tabs.length > 0) {
        const parentId = await getUberTabFolderId();
        const newFolder = await createFolder({ parentId });
        const newTab = await createTab();

        tabs.forEach((tab) => {
            createBookmark({
                title: tab.title,
                url: tab.url,
                parentId: newFolder.id,
            }).then(() => chrome.tabs.remove(tab.id));
        });
    }
});
