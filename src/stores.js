import { readable } from "svelte/store";

const getFolders = list => {
    return list.filter(l => l.children);
    // .map(l => {
    //     return {
    //         id: l.id,
    //         title: l.title,
    //         children: l.children
    //     };
    // });
};

export const folders = readable([], function start(set) {
    const getBookmarks = () => {
        chrome.storage.sync.get(["uberTabFolderId"], function(result) {
            chrome.bookmarks.getSubTree(result.uberTabFolderId, tree => {
                const filter = getFolders(tree[0].children);
                set(filter);
            });
        });
    };

    getBookmarks();

    // debugger;
    // chrome.bookmarks.onChanged.addListener(getBookmarks);

    return function stop() {
        clearInterval(interval);
    };
});
